#Requires -Modules Hyper-V
#Requires -Modules CimCmdlets

New-Variable -Option Constant -Name DEPRECATION_WARNING -Value "This module is deprecated and. Please use the 'Hyper-V.Tools' module instead."

Write-Warning $DEPRECATION_WARNING

Function Modify-SystemSettings {
    [Diagnostics.CodeAnalysis.SuppressMessage('PSUseApprovedVerbs', '', Justification='Internal function')]
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true)]
        $VMData
    )
    
    $CIMSerializer = [Microsoft.Management.Infrastructure.Serialization.CimSerializer]::Create()
    $VMDataSerialized = $CIMSerializer.Serialize($VMData, [Microsoft.Management.Infrastructure.Serialization.InstanceSerializationOptions]::None)
    $VMDataXML = [System.Text.Encoding]::Unicode.GetString($VMDataSerialized)

    $MgmtSvc = Get-CimInstance -Class "Msvm_VirtualSystemManagementService" `
                            -Namespace "root\virtualization\v2"

    Invoke-CimMethod -CimInstance $MgmtSvc -MethodName ModifySystemSettings @{SystemSettings = $VMDataXML} | Out-Null
}

Function Get-VMData {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Microsoft.HyperV.PowerShell.VirtualMachine]$VM
    )
    
    $VMData = Get-CimInstance -Namespace "root\virtualization\v2" `
                        -Class Msvm_VirtualSystemSettingData `
        | Where-Object -Property ConfigurationID -EQ $VM.Id
    
    Return $VMData
}

Function Enable-VMSyntheticKernelDebugging {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Microsoft.HyperV.PowerShell.VirtualMachine]$VM,
        [Parameter(Mandatory = $true)]
        [ValidateRange(49152, 65535)]
        [Int]$DebugPort
    )
    Begin {
        Write-Warning $DEPRECATION_WARNING
    }
    Process {
        $VMData = Get-VMData -VM $VM

        $VMData.DebugPort = $DebugPort
        $VMData.DebugPortEnabled = 1

        Modify-SystemSettings -VMData $VMData

        Return $VM
    }
}

Function Disable-VMSyntheticKernelDebugging {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Microsoft.HyperV.PowerShell.VirtualMachine]$VM
    )
    Begin {
        Write-Warning $DEPRECATION_WARNING
    }
    Process {
        $VMData = Get-VMData -VM $VM

        $VMData.DebugPortEnabled = 0

        Modify-SystemSettings -VMData $VMData

        Return $VM
    }
}

Function Get-VMSyntheticKernelDebugging {
    [CmdletBinding(PositionalBinding=$false)]
    Param (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Microsoft.HyperV.PowerShell.VirtualMachine]$VM
    )
    Begin {
        Write-Warning $DEPRECATION_WARNING
    }
    Process {
        $VMData = Get-VMData -VM $VM
        
        $Result = [PSCustomObject]@{
            "VM" = $VM;
            "Name" = $VM.Name;
            "DebugPortEnabled" = $VMData.DebugPortEnabled;
            "DebugPort" = $VMData.DebugPort
        }

        $DefaultDisplaySet = "Name", "DebugPortEnabled", "DebugPort"
        $DefaultDisplayPropertySet = New-Object System.Management.Automation.PSPropertySet('DefaultDisplayPropertySet',[string[]]$DefaultDisplaySet)
        $PSStandardMembers = [System.Management.Automation.PSMemberInfo[]]@($DefaultDisplayPropertySet)
        $Result | Add-Member MemberSet PSStandardMembers $PSStandardMembers

        Return $Result
    }
}
